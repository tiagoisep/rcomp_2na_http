

function refreshwall() {
    var request = new XMLHttpRequest();
    var board = document.getElementById("wall");

    var thisWall = document.getElementById("currWall");

    request.onload = function () {
        board.innerHTML = this.responseText;
        board.style.color = "white";
        setTimeout(refreshwall, 2000);
    };

    request.ontimeout = function () {
        board.innerHTML = "SERVER TIME OUT =(, WE WILL TRY TO CONNECT AGAIN...";
        board.style.color = "red";
        setTimeout(refreshwall, 100);
    };

    request.onerror = function () {
        board.innerHTML = "SERVER NOT RESPONDING =(, PLEASE WAIT, WE WILL TRY TO CONNECT AGAIN...";
        board.style.color = "red";
        setTimeout(refreshwall, 5000);
    };

    request.open("GET", "/wall", true);
    request.timeout = 5000;
    request.send();
}

function createWall() {
    var element = document.getElementById("setup");
    var input = element.querySelectorAll("input");
    option = input[0].value;
    var request = new XMLHttpRequest();
    console.log('create wall.:' + option);
    request.open("POST", "/wall/" + option, true);
    request.send();
    var vBoard = document.getElementById("wall");
    vBoard.innerHTML = vBoard.innerHTML + "<p>Creating wall..";

}

function goToWallUm() {
    option = "Wall Um"
    var request = new XMLHttpRequest();
    console.log('create wall.:' + option);
    request.open("POST", "/wall/" + option, true);
    request.send();
    var vBoard = document.getElementById("wall");
    vBoard.innerHTML = vBoard.innerHTML + "<p>Creating wall..";
}

function goToWallDois() {
    option = "Wall Dois"
    var request = new XMLHttpRequest();
    console.log('create wall.:' + option);
    request.open("POST", "/wall/" + option, true);
    request.send();
    var vBoard = document.getElementById("wall");
    vBoard.innerHTML = vBoard.innerHTML + "<p>Creating wall..";
}

function createMessage() {
    var element = document.getElementById("setup");
    var input = element.querySelectorAll("textarea");
    message = input[0].value;
    var element1 = document.getElementById("wall");
    var input1 = element1.querySelectorAll("input");
    wallName = input1[0].value;
    var request = new XMLHttpRequest();
    request.open("POST", "/wall/" + wallName + "/" + message, true);
    request.send();
    var board = document.getElementById("wall");
    board.innerHTML = board.innerHTML + "<p>Posting Message..";

}

function deleteMessage(option) {
    var element1 = document.getElementById("wall");
    var input1 = element1.querySelectorAll("input");
    wallName = input1[0].value;

    var request = new XMLHttpRequest();
    request.open("DEL", "/wall/" + wallName + "/" + option, true);
    request.send();
    var board = document.getElementById("wall");
    board.innerHTML = board.innerHTML + "<p>Deleting..";

}

function deleteWall() {
    var element1 = document.getElementById("wall");
    var input1 = element1.querySelectorAll("input");
    wallName = input1[0].value;
    var request = new XMLHttpRequest();
    request.open("DEL", "/wall/" + wallName, true);
    request.send();
    var board = document.getElementById("wall");
    board.innerHTML = board.innerHTML + "<p>Deleting...";

}

