package data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


public class Container {

    private static final Container instance = new Container();
    private Set<String> walls = new LinkedHashSet<>();
  
    private HashMap<String, HashMap<Integer, String>> container = new LinkedHashMap<>();

    private Container() {
        preLoad();
    }

    ;
    
    public static Container getInstance() {
        
        return instance;
    }

    public void showMessage() {
        System.out.println("I am the WallContainer Singleton!");
    }

    public boolean preLoad() {
        boolean result = false;
        result = addWall("Wall Um");
        System.err.println("1");
        result = addWall("Wall Dois");
  
        result = addMessage("Wall Um", "Olá bom dia");
        result = addMessage("Wall Um", "Boa noite");
        result = addMessage("Wall Um", "Hora do lanche");
        result = addMessage("Wall Um", "Até amanhã");
        result = addMessage("Wall Dois", "Olá");
        result = addMessage("Wall Dois", "Tudo Bem?");
        result = addMessage("Wall Dois", "Que horas são?");
        result = addMessage("Wall Dois", "Mensagem vazia");
      
        return result;
    }

    public synchronized boolean addWall(String wallName) {
        boolean ret = false;
        if (null == wallName || wallName.equals("")) {
            return false;
        }
        if (walls == null) {
            walls = new LinkedHashSet<>();
        }
        ret = walls.contains(wallName);
        if(!ret)
            walls.add(wallName);
        return ret;
    }

    public synchronized boolean delWall(String wallName) {
        container.remove(wallName);
        return walls.remove(wallName);
    }

    public synchronized HashMap<Integer, String> getMessages(String wallName) {
        return (HashMap<Integer, String>) container.get(wallName);
    }

    public synchronized boolean delMessage(String wallName, Integer messageNumber) {
        boolean result = false;
        if (!walls.contains(wallName)) {
            result = false;
        }
        HashMap<Integer, String> aWallMessages = container.get(wallName);
        if (aWallMessages == null) {
            result = false;
        }
        String removed = aWallMessages.remove(messageNumber);
        if (removed != null) {
            result = true;
        }
        return result;
    }

    public synchronized boolean addMessage(String wallName, String messageBody) {
        boolean result = false;
        int size = 0;
        if (!walls.contains(wallName)) {
            result = false;
        }

        if (container == null) {
            container = new LinkedHashMap<>();
        }
        HashMap<Integer, String> aWallMessages = container.get(wallName);
        if (aWallMessages == null) {
            aWallMessages = new HashMap<>();
            String added = aWallMessages.put(0, messageBody);
            container.put(wallName, aWallMessages);
            if (added == null) {
                return true;
            }
        } else {
            String added = aWallMessages.put(aWallMessages.size(), messageBody);
            container.put(wallName, aWallMessages);
            if (added == null) {
                return true;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String wallSet = "";
        for (String wall : walls) {
            wallSet += "Wall: " + wall + "\n";
        }
        return wallSet;
    }

    public String wallMessages() {
        String wallSet = "<tr><th>Wall</th><th>Message</th></tr>";
        HashMap<Integer, String> temp = new HashMap<>();
        for (String wall : walls) {
            temp = container.get(wall);
            if (temp != null) {
                wallSet += "<tr><td>Wall: " + wall + "</td>";
                for (Map.Entry<Integer, String> entry : temp.entrySet()) {
                    String value = entry.getValue();
                    wallSet += "<td>" + value + "</td>\n";
                    wallSet += "</tr>";
                }
            }
        }
        return wallSet;
    }

    public static void main(String args[]) throws Exception {
        Container wc = Container.getInstance();
        System.err.println(wc.toString());
        System.err.println(wc.wallMessages());

    }

    public synchronized String getFirstWall() {
        String ret = "";
        int i = 0;
        for (Iterator<String> iterator = walls.iterator(); iterator.hasNext();) {
            String next = iterator.next();
            return next;
        }
        return ret;
    }

    public Set<String> getWalls() {
        return walls;
    }

    public HashMap<String, HashMap<Integer, String>> getContainer() {
        return container;
    }
    
    

}
