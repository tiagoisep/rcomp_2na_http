package HTTP;


import data.Container;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerChat implements Runnable {

  static private final String BASE_FOLDER = "www";
    static private ServerSocket sock;
    static public String currentWall = null;
    static private Container wc = null;

    public static void setWallContainer(Container newWallContainer){
        wc = newWallContainer;
    }
    
    
    public static void main(String args[]) throws Exception {
        Socket cliSock;

        currentWall = null;
        accessesCounter = 0;
       
        for (int i = 0; i < candidatesNumber; i++) {
            candidateName[i] = "Candidate " + i;
            candidateVotes[i] = 0;
        }

        try {
            sock = new ServerSocket(80);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + 80);
            System.exit(1);
        }
        while (true) {
            cliSock = sock.accept();
            Request req = new Request(cliSock, BASE_FOLDER);
            req.start();
            incAccessesCounter();
        }
    }

    private static final int candidatesNumber = 4;
    private static final String[] candidateName = new String[candidatesNumber];
    private static final int[] candidateVotes = new int[candidatesNumber];
    private static int accessesCounter;

    private static synchronized void incAccessesCounter() {
        accessesCounter++;
    }


    public static synchronized String getMessages() {

        if (null == currentWall) {
            currentWall = wc.getFirstWall();
        }

        String textHtml = "<hr>Current wall: " + " <input type=text name='" + currentWall + "' value='" + currentWall + "' readonly><hr><ul>";
        HashMap<Integer, String> messages = new HashMap<Integer, String>();
        messages = wc.getMessages(currentWall);
        //
        if (messages == null || messages.size() == 0) {
            textHtml += "<hr>Remove Current Wall<br>";
            textHtml += "<button type=button onclick=deleteWall()> remove wall  </button> ";
            textHtml = textHtml + "</ul><hr><p>HTTP server accesses counter: " + accessesCounter + "</p>"; 
                    
        } else {            
            //
            for (Map.Entry<Integer, String> entry : messages.entrySet()) {
                Integer key = entry.getKey();
                String value = entry.getValue();
                textHtml = textHtml + "<li><input type=text name=" + key + " value='" + value + "' readonly> ";
                textHtml += "<button type=button onclick=deleteMessage(" + key + ")> remove message " + key + "</button> </li>";
            }
            textHtml += "<hr>Remove Current Wall<br>";
            textHtml += "<button type=button onclick=deleteWall()> remove wall  </button> ";
            textHtml = textHtml + "</ul><hr><p>HTTP server accesses counter: " + accessesCounter + "</p>";            
        }
        return textHtml;
    }

    public static synchronized void setCurrentWall(String wall) {
        currentWall = wall;
        wc.addWall(wall);
    }

    public static synchronized void setMessage(String message) {
        wc.addMessage(currentWall, message);
    }

    public static synchronized void delMessage(String wall,String message) {
        int cN;
        try {
            cN = Integer.parseInt(message);
        } catch (NumberFormatException ne) {
            return;
        }
        //
        wc.delMessage(wall, cN);
    }

    public static synchronized void delWall(String wall) {
        currentWall = null;
        wc.delWall(wall);
    }

    public void run()  {

        Socket cliSock = null;

        currentWall = null;
        accessesCounter = 0;

        for (int i = 0; i < candidatesNumber; i++) {
            candidateName[i] = "Candidate " + i;
            candidateVotes[i] = 0;
        }

        try {
            sock = new ServerSocket(80);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + 80);
            System.exit(1);
        }
        while (true) {
            try {
                cliSock = sock.accept();
            } catch (IOException ex) {
                Logger.getLogger(ServerChat.class.getName()).log(Level.SEVERE, null, ex);
            }
            Request req = new Request(cliSock, BASE_FOLDER);
            req.start();
            incAccessesCounter();
        }

        
    }
}
