package HTTP;


import java.io.*;
import java.net.Socket;


public class Request extends Thread {

    String baseFolder;
    Socket sock;
    DataInputStream inS;
    DataOutputStream outS;

    public Request(Socket s, String f) {
        baseFolder = f;
        sock = s;
    }

    @Override
    public void run() {
 
           try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            Message request = new Message(inS);
            Message response = new Message();
            if (request.getMethod().equals("GET")) {
                if (request.getURI().equals("/wall")) {
                    response.setContentFromString(
                            ServerChat.getMessages(), "text/html");
                    response.setResponseStatus("200 Ok");
                } else {
                    String fullname = baseFolder + "/";
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (response.setContentFromFile(fullname)) {
                        response.setResponseStatus("200 Ok");
                    } else {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html; charset=UTF-8");
                        response.setResponseStatus("404 Not Found");
                    }
                }
            } else if (request.getMethod().equals("DEL") && request.getURI().startsWith("/wall/")) {
                int occurance = replaceSpaces(request.getURI()).length() - replaceSpaces(request.getURI()).replace("/", "").length();
                
                if (occurance == 2) { 
                    ServerChat.delWall(replaceSpaces(request.getURI()).substring(6));
                    response.setContentFromString("", "text/html; charset=UTF-8");
                    response.setResponseStatus("200 Ok");
                } else {
                    ServerChat.delMessage(replaceSpaces(request.getURI()).substring(6,replaceSpaces(request.getURI()).lastIndexOf("/")),replaceSpaces(request.getURI()).substring(replaceSpaces(request.getURI()).lastIndexOf("/")+1));
                    response.setContentFromString("", "text/html; charset=UTF-8");
                    response.setResponseStatus("200 Ok");
                }
            } else if (request.getMethod().equals("POST") && request.getURI().startsWith("/wall/")) {
                if (request.getURI().chars().filter(ch -> ch == '/').count() == 2) {
                    String tmp = request.getURI();
                    ServerChat.setCurrentWall(replaceSpaces(request.getURI().substring(6)));
                    response.setResponseStatus("200 Ok");
                } else if (request.getURI().chars().filter(ch -> ch == '/').count() > 2) {
                    String tmp = request.getURI();
                    ServerChat.setMessage(replaceSpaces(request.getURI().substring(request.getURI().lastIndexOf("/") + 1)));
                    response.setResponseStatus("200 Ok");
                }
            } else {
                response.setContentFromString(
                        "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                        "text/html; charset=UTF-8");
                response.setResponseStatus("405 Method Not Allowed");
            }
            response.send(outS);
        } catch (IOException ex) {
            System.out.println("Thread error when reading request");
        }

        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
        
}
     private String replaceSpaces(String origin) {
        return origin.replace("%20", " ");
    }
}