package HTTP;

import data.Container;

import java.io.IOException;
import udpsrv.UDPServer;

public class Runner {
    private static Container wc = Container.getInstance();
    
    private static void runHttp() throws IOException{ 
        ServerChat server = new ServerChat();
        server.setWallContainer(wc);
        (new Thread(server)).start();
        
        System.err.println("---The HTTP server is running---");
    
        
    }
     private static void runUdp() throws IOException{
        UDPServer server = new UDPServer();
        server.setWallContainer(wc);
        (new Thread(new UDPServer())).start();
        
        System.err.println("---The UDP server is running---");
      
    }
    
    public static void main(String args[]) throws Exception {
        System.err.println("1");
        runHttp();
        System.err.println("2");
        runUdp();
        System.err.println("3");
    }    
}
