package udpsrv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class UDPClient {

    private static InetAddress IPdestino;
    private static int port;

    public static void main(String[] args) throws IOException {
        List<String> listPost = new ArrayList();
        List<String> listWall = new ArrayList();
        String nomePost;
        String nomeWall;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Insira o Post:\n");
        nomePost = in.readLine();

        System.out.print("Qual o nome da Wall pretendida?:\n");
        nomeWall = in.readLine();

        listWall.add(nomeWall);

        for (int i = 0; i < listPost.size(); i++) {
            byte[] post_bytes = listPost.get(i).getBytes();
            DatagramSocket sock = new DatagramSocket();
            DatagramPacket post_packet = new DatagramPacket(post_bytes, post_bytes.length, IPdestino, port);
            sock.send(post_packet);

            sock.close();

        }

        for (int j = 0; j < listWall.size(); j++) {

            byte[] wall_bytes = listWall.get(j).getBytes();
            DatagramSocket sock = new DatagramSocket();
            DatagramPacket wall_packet = new DatagramPacket(wall_bytes, wall_bytes.length, IPdestino, port);
            sock.send(wall_packet);

            sock.close();

        }

    }

}
