package udpsrv;

import data.Container;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPServer implements Runnable {
    static DatagramSocket socket;
    private Container wallCont;
    private Set<String> walls;
    private HashMap<String, HashMap<Integer, String>> tempCont = new LinkedHashMap<>();
    private final ArrayList<String> arrayPosts = new ArrayList<>();
    private final ArrayList<String> arrayWalls = new ArrayList<>();

    @Override
    public void run() {
        byte[] dados = new byte[1];
        boolean port_check = false;
        int port = 0;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        DatagramPacket udpP = new DatagramPacket(dados, dados.length);

        System.out.println("A verificar pedidos...\n");

        
        
        while (port_check == false) {
            try {
                System.out.println("Digite a porta pff\n");
                
                port = in.read();
                if (port >= 1 && port <= 32132) {
                    port_check = true;
                }
            } catch (IOException ex) {
                Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            socket = new DatagramSocket(port);
        } catch (BindException ex) {
            System.out.println("Bind à porta local falhou.");
            System.exit(1);
        } catch (SocketException ex) {
            System.out.println("Bind à porta local falhou.");
            System.exit(1);
        }

        System.out.println("A escutar pedidos\n");

        
        while (true) {
            try {
                socket.receive(udpP);

                System.out.println("Recebido pedido de: " + udpP.getAddress().getHostAddress() + " com porta: " + udpP.getPort());

            } catch (IOException ex) {
                System.exit(1);
            }

            addMessage();
        }
    }

   public void setWallContainer(Container wc) {
        this.wallCont = wc;
    }

    
    public void addMessage() {
        int i = 0;
        walls = wallCont.getWalls();
        while (i < arrayPosts.size()) {
            String tempWall = (String) arrayWalls.get(i);
            if (!walls.contains(tempWall)) {
                HashMap<Integer, String> aWallMessages = tempCont.get(tempWall);
                String wall_post = (String) arrayPosts.get(i);
                if (tempCont == null) {
                    tempCont = new HashMap<>();
                    String added = aWallMessages.put(0, wall_post);
                    tempCont.put(tempWall, aWallMessages);
                } else {
                    String added = aWallMessages.put(aWallMessages.size(), wall_post);
                    tempCont.put(tempWall, aWallMessages);
                }
            }
            i++;
        }
    }
}
